import re
from enum import StrEnum, auto
from typing import Optional, List
from uuid import UUID

from pydantic import BaseModel, Field, validator

from config import settings


class Health(BaseModel):
    service_name: str = settings.app_name
    version_number: str = settings.version_number
    errors: List[str]


class JsonPatchOperations(StrEnum):
    add = auto()  # type: ignore
    remove = auto()  # type: ignore
    replace = auto()  # type: ignore
    move = auto()  # type: ignore
    copy = auto()  # type: ignore
    test = auto()  # type: ignore


class JsonPatch(BaseModel):
    """
    RFC 6902 - JavaScript Object Notation (JSON) Patch

    This represents a single line of the patch, the standard calls for a JSON Array of these.
    """

    value: Optional[object]
    path: str
    from_path: Optional[str] = Field(None, alias="from")
    op: JsonPatchOperations

    @validator("path")
    def check_path(cls, path):
        if not re.match(r"(^/\S*$)", path):
            raise ValueError("invalid path")
        return path

    @validator("from_path")
    def check_from_path(
        cls, from_path, values, **kwargs
    ):  # pylint:disable=unused-argument
        if values is None:
            values = {}
        if not re.match(r"(^/\S*$)", from_path) and from_path != values["path"]:
            raise ValueError("invalid path")
        return from_path

    @validator("op")
    def check_op(cls, op, values, **kwargs):  # pylint:disable=unused-argument
        if values is None:
            values = {}
        if op in ("move", "copy") and "from_path" not in values:
            raise ValueError("missing from")
        return op


class Genre(BaseModel):
    genre_uid: str
    name: str


class UserPost(BaseModel):
    oauth_id: str
    username: Optional[str] = ""
    first_name: Optional[str] = ""
    last_name: Optional[str] = ""
    bio: Optional[str] = ""
    profile_picture: Optional[str] = ""


class UserResponse(UserPost):
    uid: UUID
    oauth_id: str
    preferences: Optional[object] = {}
    spotify_authorization_token: Optional[str]
    apple_music_user_token: Optional[str]

    class Config:
        orm_mode = True

    @classmethod
    def from_orm(cls, obj):
        result = super().from_orm(obj)
        result.spotify_authorization_token = getattr(
            getattr(obj, "spotify_access", None), "authorization_token", None
        )
        result.apple_music_user_token = getattr(
            getattr(obj, "apple_music_access", None), "music_user_token", None
        )
        return result


class PatchResponse(BaseModel):
    count: int
    successes: int
    errors: List[str]
