from typing import Optional, List
from uuid import UUID

from fastapi import APIRouter, HTTPException
from pony.orm import db_session

from database.models import UserDB
from v1.models import (
    UserResponse,
    JsonPatchOperations,
    JsonPatch,
    UserPost,
    PatchResponse,
)
from v1.patch_operations import patch_add, patch_remove, patch_replace

router = APIRouter(
    prefix="/v1",
    tags=["v1"],
)


@router.post(
    "/user",
    response_model=UserResponse,
    response_model_exclude_none=True,
    status_code=201,
)
async def post_user(body: UserPost):
    with db_session:
        if UserDB.get(oauth_id=body.oauth_id) is not None:
            raise HTTPException(status_code=409, detail="user already exists")
        try:
            user = UserDB(**body.dict(exclude_none=True))
        except ValueError as e:
            raise HTTPException(
                status_code=400, detail="missing or invalid field"
            ) from e
        return UserResponse.from_orm(user)


@router.get(
    "/user",
    response_model=UserResponse,
    response_model_exclude_none=True,
    response_model_exclude_defaults=True,
    status_code=200,
)
async def get_user(
    oauth_id: Optional[str] = None,
    username: Optional[str] = None,
):
    valid_input_field = False
    for param in [oauth_id, username]:
        if isinstance(param, (str, UUID)):
            valid_input_field = True
            break
    if not valid_input_field:
        raise HTTPException(status_code=404, detail="missing required parameter")
    with db_session:
        user_raw: Optional[UserDB]
        if oauth_id is not None:
            user_raw = UserDB.get(oauth_id=oauth_id)
        elif username is not None:
            user_raw = UserDB.get(username=username)
        else:
            raise HTTPException(status_code=404, detail="User not found")
        return UserResponse.from_orm(user_raw)


@router.get("/user/{uid}", response_model=UserResponse, status_code=200)
async def get_user_by_uid(uid: str):
    with db_session:
        if (user_raw := UserDB.select(lambda u: u.uid == uid).first()) is None:
            raise HTTPException(status_code=404, detail="User not found")
        return UserResponse.from_orm(user_raw)


@router.patch("/user/{uid}", response_model=PatchResponse, status_code=200)
async def patch_user_data(uid: UUID, body: List[JsonPatch]):
    with db_session:
        if UserDB.get(uid=uid) is None:
            raise HTTPException(status_code=404, detail="Listing not found")
        errors = []
        for patch in body:
            try:
                match patch.op:
                    case JsonPatchOperations.add:
                        await patch_add(uid, patch)
                    case JsonPatchOperations.remove:
                        await patch_remove(uid, patch)
                    case JsonPatchOperations.replace:
                        await patch_replace(uid, patch)
                    case _:
                        raise ValueError(f"unsupported operation: '{patch.op}' ")
            except ValueError as e:
                errors.append(str(e.args))
        return PatchResponse(
            count=len(body), successes=len(body) - len(errors), errors=errors
        )
