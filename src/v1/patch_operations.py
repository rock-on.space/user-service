from uuid import UUID

from .models import JsonPatch
from database.models import UserDB, SpotifyAccessDB, AppleMusicAccessDB


async def patch_replace(user_uid: UUID, patch: JsonPatch):
    user = UserDB.select(lambda u: u.uid == user_uid).first()
    match patch.path:
        case "/username":
            if not isinstance(patch.value, str):
                raise ValueError()
            user.username = patch.value
        case "/first_name":
            if not isinstance(patch.value, str):
                raise ValueError()
            user.first_name = patch.value
        case "/last_name":
            if not isinstance(patch.value, str):
                raise ValueError()
            user.last_name = patch.value
        case "/bio":
            if not isinstance(patch.value, str):
                raise ValueError()
            user.bio = patch.value  # ToDo: apply moderation filters
        case "/profile_picture":
            if not isinstance(patch.value, str):
                raise ValueError()
            user.profile_picture = patch.value  # ToDo: sanitize path
        case _:
            raise ValueError(
                f"unrecognized operation ({patch.op}) for path ({patch.path})"
            )


async def patch_add(user_uid: UUID, patch: JsonPatch):
    user = UserDB.select(lambda u: u.uid == user_uid).first()
    match patch.path:
        case "/username":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            if user.get(username=patch.value) is not None:
                raise ValueError("username taken")
            user.username = patch.value
        case "/first_name":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            user.first_name = patch.value
        case "/last_name":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            user.last_name = patch.value
        case "/bio":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            user.bio = patch.value  # ToDo: apply moderation filters
        case "/profile_picture":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            user.profile_picture = patch.value  # ToDo: sanitize path
        case "/spotify_authorization_token":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            if user.spotify_access is not None:
                raise ValueError("conflict: existing spotify_access")
            SpotifyAccessDB(authorization_token=patch.value, user=user)
        case "/apple_music_user_token":
            if not isinstance(patch.value, str):
                raise ValueError(
                    f"invalid value ({patch.value}) for path ({patch.path})"
                )
            if user.apple_music_access is not None:
                raise ValueError("conflict: existing apple_music_access")
            AppleMusicAccessDB(music_user_token=patch.value, user=user)
        case _:
            raise ValueError(
                f"unrecognized operation ({patch.op}) for path ({patch.path})"
            )


async def patch_remove(user_uid: UUID, patch: JsonPatch):
    user = UserDB.select(lambda u: u.uid == user_uid).first()
    match patch.path:
        case "/profile_picture":
            user.profile_picture = ""
        case "/spotify_authorization_token":
            if (spotify_access := user.spotify_access) is None:
                return
            spotify_access.delete()
        case "/apple_music_user_token":
            if (apple_music_access := user.apple_music_access) is None:
                return
            apple_music_access.delete()
        case _:
            raise ValueError(
                f"unrecognized operation ({patch.op}) for path ({patch.path})"
            )
