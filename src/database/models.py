from uuid import UUID

from pony.orm import Database
from pony.orm import PrimaryKey as PonyPrimaryKey
from pony.orm import Required as PonyRequired
from pony.orm import Optional as PonyOptional

from config import settings

db = Database()


class UserDB(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(UUID, auto=True)
    oauth_id = PonyRequired(str)
    username = PonyOptional(str, unique=True)
    first_name = PonyOptional(str)
    last_name = PonyOptional(str)
    bio = PonyOptional(str)
    profile_picture = PonyOptional(str)
    spotify_access = PonyOptional("SpotifyAccessDB")  # type: ignore
    apple_music_access = PonyOptional("AppleMusicAccessDB")  # type: ignore


class SpotifyAccessDB(db.Entity):  # type: ignore
    id = PonyPrimaryKey(int, auto=True)
    authorization_token = PonyRequired(str, unique=True)
    user = PonyRequired(UserDB)


class AppleMusicAccessDB(db.Entity):  # type: ignore
    id = PonyPrimaryKey(int, auto=True)
    music_user_token = PonyRequired(str, unique=True)
    user = PonyRequired(UserDB)


match settings.db_provider:
    case "sqlite":
        db.bind(
            provider=settings.db_provider, filename=settings.db_name, create_db=True
        )
    case "postgres":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case "cockroach":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case _:
        raise ValueError("Unknown database provider")


db.generate_mapping(create_tables=True)
